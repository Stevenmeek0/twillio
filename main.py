# Download the helper library from https://www.twilio.com/docs/python/install
import os
import json
import requests
from twilio.rest import Client
from colorama import Fore, Back, Style


# Your Account Sid and Auth Token from twilio.com/console
# and set the environment variables. See http://twil.io/secure
account_sid = ''
auth_token = ''
client = Client(account_sid, auth_token)



#crates objects used for padding strings
class Padding:
	def __init__(self, padding_count, padding_char = " "):
		self.padding_count = padding_count
		self.padding_char = padding_char

	def padding(self, string):
		padding = self.padding_count - len("".join(string))
		return padding * padding_char


#reads numbers from json file or database
def read_numbers():
	#reads numbers from json file
	with open("numbers.json") as file:
		data = json.load(file)

	return data["numbers"]


def main(): 
	#load numbers in memory
	numbers = read_numbers()
	#set the ammount of chars from the first letter of the first string to the first letter of the last string
	call_padding = Padding(30)
	for contact in numbers:
		print("Calling", contact["name"], contact["number"], end='')
		padding = call_padding.padding(["Calling", contact["name"], contact["number"]])
		try:
			error()
			call = client.calls.create(
								 from_='+447782339745',
								 
								 to=contact["number"],
								 url="https://comp-server.uhi.ac.uk/~17030095/voice.xml"
							 )
		except Exception:
			print(padding, Fore.RED, "ERROR", Style.RESET_ALL)
		else:
			print(padding, Fore.GREEN, "Pass", Style.RESET_ALL)

if __name__ == "__main__":
	main()